# TI MSP430 MIDI out interface

## Introduction

This is a simple demo code to demonstrate MIDI out capability over UART using a TI MSP430G2553 microcontroller.

It has been tested on a TI Launchpad MSP-EXP430G2ET development kit, plugged on a Roland U-220 rack synthetiser.

## Circuit diagram

```
    MIDI Female DIN
     (Front view)

         -
     3       1
      5     4
      |  2  |
      |  |  |
      |  |  +-------[ 33 Ohms ]--- +3.3V
      | GND
      |                             ------------------
      +---------[ 10 Ohms ]--------| P1.2 MSP430      |
                                   |                  |
```

## Description
The pin 5 of the MIDI DIN female connector is connected to the pin 1.2 of the MSP430 through a 10 Ohms resistor.  
The pin 4 is connected to the +3.3V rail through a 33 Ohms resistor.  
The pin 2 is connected to the ground.  
Pins 3 and 5 are not connected.

For reference, check the [MIDI 1.0 Electrical Specification](https://www.midi.org/specifications-old/item/midi-din-electrical-specification).

## Code
The code is reduced to a minimum for the sake of clarity, triggering the same note within an infinite loop.  

I decided to publish this code as many examples found online are incorrect, mostly regarding the baud rate.
