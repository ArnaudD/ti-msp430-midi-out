//******************************************************************************
//
// Texas Instruments MSP430G2553 - MIDI OUT implementation via UART
//
//******************************************************************************
//
//    MIDI Female DIN
//     (Front view)
//
//         -
//     3       1
//      5     4
//      |  2  |
//      |  |  |
//      |  |  +-------[ 33 Ohms ]--- +3.3V
//      | GND
//      |                             ------------------
//      +---------[ 10 Ohms ]--------| P1.2 MSP430      |
//                                   |                  |
//
//  Arnaud Durand
//  Octobre 2023

//******************************************************************************

#include <msp430.h>

int main(void)
{
    unsigned long i;                    // For timer
    WDTCTL = WDTPW + WDTHOLD;           // Stop watchdog timer

    // Set internal DCO at 1MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    // UART TX selection on P1.2
    P1SEL |= BIT2;
    P1SEL2 |= BIT2;

    // UCA0 reset configuration on
    UCA0CTL1 = UCSWRST;

    // Configure UCA0
    UCA0CTL1 |= UCSSEL_2;   // SMCLK
    UCA0BR0 = 32;           // Divider from 1MHz to get 31250 bauds
    UCA0BR1 = 0;            // Divider from 1MHz to get 31250 bauds
    UCA0MCTL = UCBRS0;      // Modulation UCBRSx = 1

    // UCA0 out of reset configuration
    UCA0CTL1 &= ~UCSWRST;

    // Main loop
    while(1)
    {
        while (!(IFG2 & UCA0TXIFG)); // Wait until buffer is ready
        UCA0TXBUF = 0x90;            // Status byte: Note ON, MIDI channel 1

        while (!(IFG2 & UCA0TXIFG)); // Wait until buffer is ready
        UCA0TXBUF = 0x34;            // Data byte: Send Note number

        while (!(IFG2 & UCA0TXIFG)); // Wait until buffer is ready
        UCA0TXBUF = 0x30;            // Data byte: Velocity

        // Timer
        for (i=0; i<100000; i++);
  }
}
